#!/usr/bin/env ruby

require 'pony'

items = {} # key: 'value' as  {ya.ru: 'search'}

items.each {|key, value| Pony.mail(
  :to => '',
  :from => '',
  :subject => 'Warning!!!!',
  :body => "#{key} - не  работает!",
  :via => :smtp,
  :via_options => {
      :address     => '',
      :port     => '',
      :user_name     => '',
      :password => '',
      :auth     => :plain,           # :plain, :login, :cram_md5, no auth by default
      :domain   => "",
      :enable_starttls_auto => true,
      # :openssl_verify_mode  => 'none'
    }
) unless `curl #{key}`.include? value}

